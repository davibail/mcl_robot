*If only there was a way to live all those adventures I dream of at night... Wait there is ???*

M. A. P. which stands for More Adventure Please is a quick game for one or more player in which they take a character into a custom adventure. I know, nothing very groundbreaking here but I find it more fun to live the adventure rather than just tell or read it. So, with a fex cards and tokens, why not jump into a fascinating adventure with friends or familly ?


## WHAT YOU NEED TO PLAY
- The included cards depicting characters, places, enemies, events, objects, etc... (there are a lot and could be more in the future! stay tunned)
- Some dices
- If you don't like the custom printable tokens (it's your choice), some tokens to track characters and others stats


## SETUP
I don't fully know yet but something in the lines of:
Build or draft 9 cards decks
Setup the cards in the following fashion:
(some picture)
Place your tokens on the starting card
Draw 5 cards and mulligan once the cards you don't want.
Reveal the starting card


## PLAY
Decide who is the leader of the group.

Each turn you will in sequence do:
 1. Effects step: Cards engaged with characters do their effect
 2. Move step: Cards that track characters move toward them
 3. Player step: Players can play (yeah, I know, isn't that crazy !?!)
 4. Upkep step: Players get one resource and draw one card
 

## PLAYER STEP
During your turn you have 3 actions to take by default among the following (you can take the same action multiple times): 
 - Draw one card
 - Gain one resource
 - Pay the cost and play a card for it's effect
 - Move to a different location
 - Solve (or at least try) a puzzle or challendge listed on a card (location or other)
 
 
## KEYWORDS
There are some keywords for more concise card text. Here they are (hopefully all of them):
- Fast: this doesn't take an action to execute
- Ongoing: doesn't need to be triggered, is always true or is true when the conditions are met
- Track: will track a target described in the text
- Chose one: you need to chose one (yeah, I probably didn't need to add this one)
- Reaction: will trigger when an unsuccessfull action is made regarding this card 


## END OF THE GAME
The game ends when the furthest card from the starting one is solved.